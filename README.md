# sgi_test_agstandridge

SGI Programming Test for Aaron Standridge

Hi, and thanks for taking a look at my programming test.  You'll find each 
problem and its related files in separate folders, as detailed below.

1.) People Counter

Live example: http://www.bitwrangler.net/scientific/people-counter/

FILES
index.php                    [ dataset analysis and report ]
dataset.php                  [ dataset operations, view/create dataset ]
population.json              [ current dataset ]
people-counter.css
output.txt                   [ sample program output ]

GETTING STARTED

To see the analysis tool in action, proceed to the URL above or arrange for
the code to be accessible via the webserver of your choice.  Population.json
contains the current dataset, and can be viewed, rebuilt, or downloaded
at any time via the Dataset Operations page at dataset.php.

A rough synopsis of the processing method:
 * Loop through dataset records, build maps of birth/death tallies by year
 * Create and sort a merged list of years from map keys
 * Per year, sequentially: add births, check totals, remove deaths
 * Loop until end or not enough births remain to elevate a new winner


2.) Slot Spin Endpoint

Live example: http://www.bitwrangler.net/scientific/shots-n-slots/

SERVER FILES
spin-endpoint.php            [ server endpoint for spins ]
sgi_test_db.xml              [ MySQL schema ]
database.php                 [ Class file - primary database handler ]
player-record.php            [ Class file - player data storage ]
spin-request.php             [ Class file - spin request data storage ]
messaging.php                [ helpers for responding, error reporting, et al ]

DEMO CLIENT FILES
index.php                    [ client mockup/testing platform ]
reset.php                    [ convenience script for resetting player stats ]
sjcl.js                      [ open javascript hashing library ]
slots.css
images/*.*                   [ slot demo images ]

GETTING STARTED

A demo of the endpoint is provided at the URL above, and/or the project can be
reconstructed on the webserver of your choice.  

On receiving a spin, the server retrieves the player record, authenticates the
client via a hash of their playerID and a salt value known only to the client,
then validates the spin request data, which consists roughly of:

 * coins bet (is numeric, >= 0, <= player->credits, <= server->max_bet)
 * coins won (is numeric, >= 0, <= server->max_payout)

Once deemed valid, net winnings are calculated as (coins won - coins bet), and
the player record is updated with that and an incremented lifetime spins count.

If everything is successful to this point, a JSON response is generated with the
requested data.  If instead, an error is encountered or the spin is invalid,
an error code is sent to the client to deal with as it sees fit.

Contents of the DEMO CLIENT FILES section are not strictly necessary to the test,
but provide a convenient means of testing the endpoint, and can be found at the
URL above.  Values in the mockup client are shamelessly hardcoded in several
key places (e.g. playerID and salt), and it doesn't make any pretense of basing
winnings on spin results, instead being purely random.
