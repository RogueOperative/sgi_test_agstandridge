<?php
//
// Dataset creation helpers
//
DEFINE("DATASET_LENGTH", 5000);
DEFINE("DATASET_STARTYEAR", 1900);
DEFINE("DATASET_ENDYEAR", 2000);
DEFINE("FILENAME", "population.json");

if (isset($_REQUEST['createdataset']))
{
    create_dataset();
}

$raw_json = file_get_contents(FILENAME);
$dataset = json_decode(utf8_encode($raw_json));

?>
<html>
    <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <title>Low-Frills People Counter Dataset Operations</title>
        <link rel="stylesheet" type="text/css" href="people-counter.css" />
    </head>
    <body>
        <div id="content">
            <h2>Dataset Operations</h2>
            <a href="dataset.php?viewdataset=1">View Dataset</a> (<?php echo(count($dataset)); ?> Entries)
            <br/><br/>
<?php
// Give them the option to easily download raw JSON file
if (file_exists(FILENAME))
{
?>
            <a href="<?php echo FILENAME; ?>">View/Download Raw Json Dataset</a> (<?php echo filesize(FILENAME); ?> bytes)
            <br/>
            <span style="font-size:80%;">NB: clear your cache or reload the page if the json file seems stuck!</span>
            <br/><br/>
<?php
}
?>
            <a href="dataset.php?createdataset=1&datasetsize=<?php echo DATASET_LENGTH ?>">Create New <?= DATASET_LENGTH ?> Entry Dataset</a>
            <br/><br/>
            <a href="index.php">Return to Analyzer</a>
            <br/><br/>
<?php
if (isset($_REQUEST['viewdataset']) || isset($_REQUEST['createdataset']))
{
    echo("<h3>Current Dataset</h3>");
    for($dataindex = 0; $dataindex < count($dataset); $dataindex++)
    {
        $current_record = $dataset[$dataindex];
        echo($current_record->name . " [b. " . $current_record->birth . " ... d. " . $current_record->death . "]<br/>");
    }
}
?>
        </div>
    </body>
</html>

<?php
function create_dataset()
{
    $datasetLength = isset($_REQUEST['datasetsize']) ? $_REQUEST['datasetsize'] : DATASET_LENGTH;

    // initialize our data file
    file_put_contents(FILENAME, "[");

    // Lists of random names generated via http://listofrandomnames.com!
    $firstnames = array("Emilee", "Aundrea", "Shanon", "Melodi", "Valorie", "Rheba", "Corene",
        "Deandre", "Yasuko", "Elfriede", "Sharee", "Adam", "Bari", "Aida", "Marcellus", "Lisa",
        "Bryanna", "Vicenta", "Jeri", "Reta", "Willene", "Bebe", "Lynn", "Tressie", "Felicita",
        "Danuta", "Min", "Moses", "Kurtis", "Nichelle", "Indira", "Verdell", "Julius", "Ashly",
        "Seth", "Hayley", "Susanna", "Britany", "August", "Loren", "Brittani", "Tessa", "Nga",
        "Buck", "Margrett", "Marlin", "Stefany", "Voncile", "Eliseo", "Darla");

    $lastnames = array("Vossen", "Remaley", "Rittenberry", "Puglisi", "Benham", "Haugland",
        "Troester", "Dobos", "Sickles", "Parrilla", "Allgeier", "Spicer", "Bate", "Quinteros",
        "Ferra", "Hamid", "Varughese", "Casler", "Zemke", "Mitchem", "Abasta", "Ishmael", "Pardon",
        "Larabee", "Cubbage", "Mantyla", "Devivo", "Sand", "Culbreath", "Muscarella", "Vannoy",
        "Berkley", "Bizier", "Secrest", "Durante", "Murphy", "Burke", "Barone", "Olivier",
        "Lisowski", "Daves", "Fullenwider", "Hamblen", "Putz", "Aarons", "Voight", "Lipsky",
        "Hurd", "Tomberlin", "Corley");

    $output_format = "{\"name\":\"%s\",\"birth\":%d,\"death\":%d}";
    $max_firstname = count($firstnames) - 1;
    $max_lastname = count($lastnames) - 1;

    for ($dataindex = 0; $dataindex < $datasetLength; $dataindex++)
    {
        // just give them a random name; Elfriede Muscarella is my favorite so far
        $name = $firstnames[rand(0,$max_firstname)] . " " . $lastnames[rand(0,$max_lastname)];

        // birth year can be anything START to END
        $birth = rand(DATASET_STARTYEAR, DATASET_ENDYEAR);

        // death year needs to be chopped to ensure death no later than ENDYEAR
        $death = $birth + rand(0,(DATASET_ENDYEAR - $birth));

        $record = sprintf($output_format, $name , $birth, $death) . ($dataindex == ($datasetLength - 1) ? "]\n" : ",");
        file_put_contents(FILENAME, $record, FILE_APPEND);
    }
}
?>