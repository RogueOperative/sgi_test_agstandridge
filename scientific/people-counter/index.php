<?php
DEFINE("FILENAME", "population.json");
?>
<html>
    <head>
        <title>No-Frills People Counter</title>
        <link rel="stylesheet" type="text/css" href="people-counter.css" />
    </head>
    <body>
    <div id="content">
        <center><h3>No-Frills People Counter</h3></center>
        (<a href="dataset.php">Dataset Operations</a>)
        <br/><br/>

<?php

$raw_json = file_get_contents(FILENAME);
$dataset = json_decode(utf8_encode($raw_json));
$num_records = count($dataset);

// All we want from the dataset is a tally of birth and death totals for each year
// It's more memory intensive to store them separately, but we have to be able to
// sandwich a population check between them later

$births = array();
$deaths = array();

for($dataindex = 0; $dataindex < count($dataset); $dataindex++)
{
    $current_record = $dataset[$dataindex];

    if (!array_key_exists($current_record->birth, $births))
    {
        $births[$current_record->birth] = 1;
    }
    else
    {
        $births[$current_record->birth] += 1;
    }

    if (!array_key_exists($current_record->death, $deaths))
    {
        $deaths[$current_record->death] = 1;
    }
    else
    {
        $deaths[$current_record->death] += 1;
    }
}

$population = 0;
$best_year = 0;
$highest_population = 0;
$birth_count = 0;

// We can safely cull out any years without a birth or a death
// although as dataset size increases, these will be hard to come by
$years = array_unique(array_merge(array_keys($births), array_keys($deaths)));
sort($years);

// now step through the year range, keeping running population and birth totals
foreach ($years as $year)
{
    // add fresh humans for the year
    if (array_key_exists($year, $births))
    {
        $population += $births[$year];
        $birth_count += $births[$year];
    }

    // a person alive for any part of the year counts for the whole year
    // so do high-population checks before subtracting deaths
    if ($population > 0)
    {
        if ($population > $highest_population)
        {
            $best_year = $year;
            $highest_population = $population;
        }
        // if accounting for ties, you'd check $population = $highest_population here
        // but in this case, we're going to consider the first year to be the winner
    }
    echo("Year $year: pop. $population<br/>");

    // bring out your dead
    if (array_key_exists($year, $deaths))
    {
        $population -= $deaths[$year];
    }

    // theoretically, there's a point where not enough births remain to elevate a new winner;
    // obviously you would not want to do this if your goal is to plot data for each year,
    // but we are already skipping years without birth/death events
    if (($highest_population - $population + 1) > $num_records - $birth_count)
    {
        echo("Halting at $year because there aren't enough births remaining ("
            . ($num_records - $birth_count) . ", need "
            . ($highest_population - $population + 1) . ") to take the lead.<br/>");
        break;
    }
}

echo("<br/><b>End result: " . $best_year . " with population " . $highest_population . "</b><br/>");

?>
        </div>
    </body>
</html>