<?php
// database.php
// class file for Database helper class

DEFINE("PLAYER_TABLE", "players");

class Database
{
    public $connection;

    protected $host = "mysql.bitwrangler.net";
    protected $username = "6b4e6589ce";
    protected $password = "gdckHQUKD2oq0h0jMloH";
    protected $database = "sgi_test_db";

    public function connect()
    {
        $this->connection = mysqli_connect($this->host, $this->username, $this->password);
        if ($this->connection->connect_errno)
        {
            die("Unable to connect, burn everything down.  Error: [" . $this->connection->connect_error() . "]");
        }
        $this->connection->select_db($this->database);
    }

    public function prepare($sql)
    {
        return $this->connection->prepare($sql);
    }

    public function query($sql)
    {
        return $this->connection->execute($sql);
    }

    public function disconnect()
    {
        if (isset($this->connection))
        {
            $this->connection->close();
        }
    }

    public function getPlayerRecord($playerId)
    {
        $sql = "SELECT * FROM " . PLAYER_TABLE . " WHERE `playerID` = ?";
        $stmt = $this->connection->prepare($sql);
        if (!$stmt)
        {
            reportError("ERR_PLAYER_LOAD_BAD_SQL",
                        "Unable to create prepared statement for " . $playerId
                        . " using [" . $sql . "]");
            $db->disconnect();
            exit();
        }
    
        $stmt->bind_param('s', $playerId);
        $stmt->execute();
    
        $result = $stmt->get_result();
        if (!$result)
        {
            reportError("ERR_PLAYER_LOAD_FAILED",
                        "Unable to load PlayerRecord for " . $playerId . " using [" . $sql . "]");
            $db->disconnect();
            exit();
        }
    
        $row = $result->fetch_object();
        if (!$row)
        {
            reportError("ERR_PLAYER_OBJECT_FAILED",
                        "Unable to load database row as object for " . $playerId
                        . " using [" . $sql . "]");
            $db->disconnect();
            exit();
        }
    
        $playerRecord = new PlayerRecord();
        $playerRecord->playerId = $row->playerId;
        $playerRecord->name = $row->name;
        $playerRecord->credits = $row->credits;
        $playerRecord->lifetimeSpins = $row->lifetimeSpins;
        $playerRecord->salt = $row->salt;
    
        return $playerRecord;
    }
    
    public function updatePlayerCredits(&$playerRecord, $coinDelta)
    {
        $sql = "UPDATE " . PLAYER_TABLE . "  SET `credits` = ?, `lifetimeSpins` = ? WHERE `playerID` = ?";
        $stmt = $this->connection->prepare($sql);
        if (!$stmt)
        {
            reportError("ERR_UPDATE_CREDITS_BAD_SQL",
                        "Unable to create prepared statement to update credits for " . $playerRecord->playerId
                        . " using [" . $sql . "]");
            $db->disconnect();
            exit();
        }
    
        $playerRecord->credits += $coinDelta;
        $playerRecord->lifetimeSpins += 1;
        $stmt->bind_param('iis', $playerRecord->credits, $playerRecord->lifetimeSpins, $playerRecord->playerId);
    
        if (!$stmt->execute())
        {
            reportError("ERR_UPDATE_CREDITS_FAILED",
                        "Unable to update credits for " . $playerRecord->playerId . " [" . $stmt->errno . "]");
            $db->disconnect();
            exit();
        }
        return true;
    }

    public function ResetPlayer($playerRecord)
    {
        // Reset Credits to 100 (so they can afford next spin), and lifetime Spins to 0
        $sql = "UPDATE " . PLAYER_TABLE . "  SET `credits` = 100, `lifetimeSpins` = 0 WHERE `playerID` = ?";
        $stmt = $this->connection->prepare($sql);
        if (!$stmt)
        {
            reportError("ERR_RESET_PLAYER_BAD_SQL",
                        "Unable to create prepared statement to reset " . $playerRecord->playerId
                        . " using [" . $sql . "]");
            $db->disconnect();
            exit();
        }

        $stmt->bind_param('s', $playerRecord->playerId);

        if (!$stmt->execute())
        {
            reportError("ERR_RESET_PLAYER_FAILED",
                        "Unable to reset " . $playerRecord->playerId . " [" . $stmt->errno . "]");
            $db->disconnect();
            exit();
        }

        return true;
    }
}

?>