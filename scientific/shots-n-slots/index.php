<?php
DEFINE("PLAYER_TABLE", "players");
require_once("database.php");
require_once("messaging.php");
require_once("player-record.php");

$db = new Database();
$db->connect();
$conn = $db->connection;

$playerRecord = $db->getPlayerRecord("LuckySlotsPlayer1774");
if (!$playerRecord)
{
    reportError("ERR_INVALID_PLAYER_RECORD", "Unable to load player record!");
    $db->disconnect();
    exit();
}
?>
<html>
    <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <title>Shots n' Slots - A No-Frills Slot Machine</title>
        <link rel="stylesheet" type="text/css" href="slots.css" />
        <script type="text/javascript" src="sjcl.js" ></script>
        <script>
<!--
var playerId = "LuckySlotsPlayer1774";
var salt = "73d6010d7588e803780f79e9c0b62165";
var playerCredits = <?php echo $playerRecord->credits; ?>;

var responseText;

var images = new Array(
    "images/lime-slice_256x256.png",
    "images/salt_256x256.png",
    "images/tequila-bottle_256x256.png",
    "images/bar1_256x256.png",
    "images/bar2_256x256.png",
    "images/bar3_256x256.png",
    "images/num7_256x256.png",
);

var imageRotationSpeed = 150;
var imageRotationMultiplier = 1.0;
var spinLength = 3000;
var spinnersActive = false;
var deliveryTimer;
var rotationSlowdownTimer;

var networkIssuesTimer;
var networkIssuesIndex = 0;
var networkIssuesSpeed;
var networkIssuesMsgs = Array(
    "Aaaannnyyy second now...",
    "Hey, I can't get through<br/>The server's lazy<br/>Or something's broken<br/>The network, maybe",
    "It does seem increasingly likely there is a network issue or server breakdown of some sort.",
    "Sorry, I forgot to charge the internet.",
    "If it got this far, I'd give up and reset the page. Maybe take a screenshot and publicly shame Aaron.",
    "#blessed",
    "This area closed for maintenance until further notice."
);

function setSlotImage(slotId) {
    if (spinnersActive) {
        var slotImage = document.getElementById(slotId);

        // choose and apply a random image
        var imageIndex = Math.floor(Math.random() * (images.length - 1));
        slotImage.src = images[imageIndex];

        // if the system is still in play, set up the next switch
        setTimeout( function() { setSlotImage(slotId); }, imageRotationSpeed * imageRotationMultiplier);
    }
}

function initiateSpin() {
    if (document.getElementById("spinbutton").disabled) {
        return false;
    }

    // adjust UI
    document.getElementById("spinbutton").disabled = true;
    document.getElementById("response").innerHTML = "Spinning!";

    // kick off the RPC timer
    deliveryTimer = setTimeout(deliverSpinRequest, spinLength);

    // start our spinners.  what a visual feast!
    spinnersActive = true;
    imageRotationMultiplier = 1.0;
    setSlotImage("slotimage0");
    setTimeout( function() { setSlotImage("slotimage1"); }, 350);
    setTimeout( function() { setSlotImage("slotimage2"); }, 600);
}

function deliverSpinRequest() {
    // shut everything down
    window.clearTimeout(deliveryTimer);

    // These parameters would not normally be hardcoded like this, of course.
    var coinsBet = Math.min(100, playerCredits);
    var coinsWon = Math.ceil(Math.random() * 500); // randomly win 1-500

    // Using SJCL for SHA-256 ( http://bitwiseshiftleft.github.io/sjcl/ )
    var playerHash = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(playerId + salt));

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            window.clearInterval(networkIssuesTimer);

            // slow down the image-switching madness toward the end
            rotationSlowdownTimer = setInterval( function() { imageRotationMultiplier *= 1.15; }, 100)
            document.getElementById("response").innerHTML = "Here we go!";

            responseText = this.responseText;

            // give the slots a moment to slow down
            setTimeout( function() {
                spinnersActive = false;
                window.clearInterval(rotationSlowdownTimer);
                window.clearInterval(networkIssuesTimer);
                var msg;
                var returnData = JSON.parse(responseText);
                if (returnData.hasOwnProperty('error')) {
                    msg = "[ERROR] " + returnData.error;
                } else {
                    var netReturn = coinsWon - coinsBet;
                    if (netReturn >= 0) {
                        msg = "Congratulations, <span class='callout'>" + returnData.name
                            + "</span>!  You won <span class='callout'>" + netReturn + "</span> coins!";
                    } else {
                        msg = "Oh, cruel heartbreak, <span class='callout'>" + returnData.name
                            + "</span>!  You lost <span class='callout'>" + (netReturn * -1)
                            + "</span> coins. Better luck next time!";
                    }
                    msg += "<br/>Your new credit total is <span class='callout'>" + returnData.credits + "</span>!"
                }
                document.getElementById("response").innerHTML = msg + "<br/><br/>Raw Response Text[" + responseText + "]";
                setTimeout(function() { document.getElementById("spinbutton").disabled = false; }, 1250);
                document.getElementById("welcomecredits").innerHTML = returnData.credits;
                playerCredits = returnData.credits;
                document.getElementById("spinbutton").innerHTML = "SPIN 'EM!  (" + Math.min(100, returnData.credits) + " Credits)";
            }, 2000)
        }
    }
    xmlhttp.open("POST", "spin-endpoint.php", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send("playerid=" + playerId + "&hash=" + playerHash + "&coinsbet=" + coinsBet + "&coinswon=" + coinsWon);

    networkIssuesIndex = 0;
    networkIssuesSpeed = 6000;
    networkIssuesTimer = setInterval( function() {
        if (networkIssuesIndex < networkIssuesMsgs.length) {
            document.getElementById("response").innerHTML = networkIssuesMsgs[networkIssuesIndex];
            networkIssuesSpeed += 2000;
            networkIssuesIndex++;
        } else {
            window.clearInterval(networkIssuesTimer);
        }
    }, networkIssuesSpeed);
}

function resetPlayer() {
    // Using SJCL for SHA-256 ( http://bitwiseshiftleft.github.io/sjcl/ )
    var playerHash = sjcl.codec.hex.fromBits(sjcl.hash.sha256.hash(playerId + salt));

    location.replace("reset.php?playerid=" + playerId + "&hash=" + playerHash);
}

-->
        </script>
    </head>
    <body>
        <div id="content">
            <h3>Shots n' Slots - A No-Frills Slot Machine</h3>
            <br/>
            Welcome, <span class="callout"><?php echo $playerRecord->name; ?></span>!  You have <span id="welcomecredits" class="callout"><?php echo $playerRecord->credits; ?></span> Credits.<br/><br/>Try your luck!<br/><br/>
            <span style="font-size:50%;">Results 100% random.  Any similarity to actual slots unintentional.</span>
            <div id="slots" class="slotcolumns">
                <div class="slot" id="slot0"><img class="slotimage" id="slotimage0" src="images/tequila-bottle_256x256.png"/></div>
                <div class="slot" id="slot1"><img class="slotimage" id="slotimage1" src="images/salt_256x256.png"/></div>
                <div class="slot" id="slot2"><img class="slotimage" id="slotimage2" src="images/lime-slice_256x256.png"/></div>
            </div>
            <button type="button" id="spinbutton" onclick="initiateSpin();">SPIN 'EM!  (<?php echo(min(array(100, $playerRecord->credits))); ?> Credits)</button>
            <div id="response"></div>
            <button type="button" id="resetbutton" onclick="resetPlayer();">Reset Yourself</button>
        </div>
    </body>
</html>