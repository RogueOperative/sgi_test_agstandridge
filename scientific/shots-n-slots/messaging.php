<?php
// Common-use messaging functions

function reportError($errcode, $error)
{
    $response = new stdClass();

    // Realistically, I would probably run the descriptive errors off to a logging system
    // and issue the client a relevant error code, so it can localize and whatnot
    $response->errcode = $errcode;
    $response->error = "Unable to process Spin Request [ " . $error . " ]";

    sendResponse($response);
}

function sendResponse($response)
{
    header('Content-Type: application/json');
    echo json_encode($response);
}

?>