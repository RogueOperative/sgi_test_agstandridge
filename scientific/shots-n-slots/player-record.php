<?php
// database.php
// class file for PlayerRecord  helper class

class PlayerRecord
{
    public $playerId = "";
    public $name = "";
    public $credits = 0;
    public $lifetimeSpins = 0;
    public $salt = "";
}

?>