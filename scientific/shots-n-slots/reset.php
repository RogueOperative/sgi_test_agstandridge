<?php
// reset.php
// Allows a quick and dirty way to reset yourself in the database, wiping credits and lifetime spins
// Uses same authentication as spin endpoint
//

require_once("database.php");
require_once("player-record.php");

$playerId = $_REQUEST["playerid"];
$hash = $_REQUEST["hash"];

$db = new Database();
$db->connect();
$conn = $db->connection;

$playerRecord = $db->getPlayerRecord($playerId);
if (!$playerRecord)
{
    echo("ERR_INVALID_PLAYER_RECORD : Unable to load player record from [" . $playerId . "]");
    $db->disconnect();
    exit();
}

$serverHash = hash("sha256", $playerRecord->playerId . $playerRecord->salt);
if ($serverHash !== $hash)
{
    echo("ERR_AUTH_FAILED : Invalid authentication credentials");
    $db->disconnect();
    exit();
}

?>
<html>
    <head>
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <title>Shots n' Slots Player Resetter</title>
        <link rel="stylesheet" type="text/css" href="slots.css" />
    </head>
    <body>
        <div id="content">
            <h3>Shots n' Slots Player Resetter</h3>
            <br/>
            Welcome, <?php echo $playerRecord->name; ?>...
<?php

if (!$db->ResetPlayer($playerRecord))
{
    echo("Resetting Failed!");
} else {
    $playerRecord = $db->getPlayerRecord($playerRecord->playerId);
    echo("You've been reset!  You now have " . $playerRecord->credits . " Credits and "
        . $playerRecord->lifetimeSpins . " lifetime spins.");
}

?>
            <br/><br/>
            <a href="index.php">Return to Shots n' Slots!</a>
        </div>
    </body>
</html>

