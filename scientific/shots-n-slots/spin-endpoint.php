<?php
require_once("database.php");
require_once("messaging.php");
require_once("player-record.php");
require_once("spin-request.php");

// First, let's establish our database connection, because if that's no good, nothing is good

$db = new Database();
$db->connect();
$conn = $db->connection;

// We'll need our request info in some form to get the player record
$spinRequest = new SpinRequest();
$spinRequest->playerId = $_REQUEST["playerid"];
$spinRequest->hash     = $_REQUEST["hash"];
$spinRequest->coinsBet = $_REQUEST["coinsbet"];
$spinRequest->coinsWon = $_REQUEST["coinswon"];

$playerRecord = $db->getPlayerRecord($spinRequest->playerId);
if (!$playerRecord)
{
    // Theoretically a new player would have no account, but new players shouldn't be entering the process here
    // So any failure to find an account should be an unpleasant surprise
    reportError("ERR_INVALID_PLAYER_RECORD", "Unable to load player record!");
    $db->disconnect();
    exit();
}

// We presumably authenticated account credentials at the beginning of the session,
// then generated a fresh salt that we can use with their ID to safely verify identity
$serverHash = hash("sha256", $playerRecord->playerId . $playerRecord->salt);
if ($serverHash !== $spinRequest->hash)
{
    reportError("ERR_AUTH_FAILED", "Invalid authentication credentials");
    $db->disconnect();
    exit();
}


if (!validateSpinRequest($spinRequest, $playerRecord))
{
    $db->disconnect();
    exit();
}

// Everything appears to be on the level, so let's proceed with the awarding of loot
$netWinnings = $spinRequest->coinsWon - $spinRequest->coinsBet;

if (!$db->updatePlayerCredits($playerRecord, $netWinnings))
{
    reportError("ERR_UPDATE_CREDITS_FAILED", "Unable to update player Credits!");
    $db->disconnect();
    exit();
}

$response = new stdClass();
$response->playerId = $playerRecord->playerId;
$response->name = $playerRecord->name;
$response->credits = $playerRecord->credits;
$response->lifetimeSpins = $playerRecord->lifetimeSpins;
$response->lifetimeAverageReturn = $playerRecord->credits / $playerRecord->lifetimeSpins;

sendResponse($response);

$db->disconnect();

// Function Definitions
function validateSpinRequest(&$spinRequest, $playerRecord)
{
    // We avoid some sanitation because we're not really introducing client input to the DB
    // But we still need to be sure they aren't up to any shenanigans

    $serverOptions = getServerOptions();

    // Make sure they're not betting "banana" or somehow using negative coins;
    // zero-coin bets are allowed on the assumption there are free bonus spins or etc
    if (!(is_numeric($spinRequest->coinsBet) && $spinRequest->coinsBet >= 0))
    {
        reportError("ERR_INVALID_BET_DATA", "Bet data corrupt");
        return false;
    }

    // Don't let them bet more than they have - that's just bad business
    if($spinRequest->coinsBet > $playerRecord->credits)
    {
        // However, for test purposes, we must allow it, because we have no way to add coins
        // and we'd hate for a player to get stuck, unable to play
        if ($serverOptions->enforceInsuffucientFunds)
        {
            reportError("ERR_INSUFFICIENT_FUNDS", "Insufficient funds [need " . $spinRequest->coinsBet . " have " . $playerRecord->credits . "]");
            return false;
        } else {
            // So let's cheat and lower the bet to what they can afford
            // NB: If this protection is permanently removed, we should reevaluate
            //     passing $spinRequest by reference
            $spinRequest->coinsBet = $playerRecord->credits;
        }
    }

    // Ensure they haven't exceeded server limits in some way
    if($serverOptions->enforceBettingLimits && $spinRequest->coinsBet > $serverOptions->maxBet)
    {
        reportError("ERR_SERVER_MAX_BET_EXCEEDED", "Placed bet exceeds server max bet");
        return false;
    }

    // As with bets, winnings should presumably be a 0+ number,
    // unless the game design allows for losing more than you bet
    if (!(is_numeric($spinRequest->coinsWon) && $spinRequest->coinsWon >= 0))
    {
        reportError("ERR_INVALID_WINNINGS_DATA", "Winnings data corrupt");
        return false;
    }

    // Ensure they haven't exceeded server payout limits
    // Payout limit should be acceptably above normally obtainable payout,
    // so if it happens, consider it an outright error instead of capping payout
    if($serverOptions->enforcePayoutLimits && $spinRequest->coinsWon > $serverOptions->maxPayout)
    {
        reportError("ERR_SERVER_MAX_PAYOUT_EXCEEDED", "Payout exceeds server limits.");
        return false;
    }

    // Looks good!
    return true;
}

function getServerOptions()
{
    // Generally, I would probably pull this from a database or config file
    $serverOptions = new stdClass();

    $serverOptions->enforceBettingLimits = true;
    $serverOptions->maxBet = 1000000;

    // Call it a safety feature
    $serverOptions->enforcePayoutLimits = true;
    $serverOptions->maxPayout = 5000000;

    // Allows players to cheat for demonstration purposes
    $serverOptions->enforceInsuffucientFunds = false;
}
?>